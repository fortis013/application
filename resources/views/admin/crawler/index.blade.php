<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title"></h3>
					<div class="box-tools"></div>
				</div>

				<div class="box-body">
					<div class="form-horizontal">
						<div class="fields-group">
							<div class="form-group">
								<label for="api_url" class="col-sm-2 control-label">爬虫登入 API</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="api_url" name="api_url" class="form-control" value="http://192.168.1.246:1048/loginGet" placeholder="输入 爬虫登入 API">
									</div>
								</div>
							</div>
						</div>
					</div><!-- .form-horizontal -->
					<hr>

					<form class="form-horizontal">

						<div class="fields-group">
							<div class="form-group">
								<label for="domain_name" class="col-sm-2 control-label">Domain name</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="domain_name" name="domain_name" class="form-control" value="https://cj168.01jsdc.com/" placeholder="输入 Domain name">
									</div>
								</div>
							</div>
						</div>

						<div class="fields-group">
							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">帐号</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="username" name="username" class="form-control" value="qpchshi" placeholder="输入 帐号">
									</div>
								</div>
							</div>
						</div>

						<div class="fields-group">
							<div class="form-group">
								<label for="password" class="col-sm-2 control-label">密码</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="password" id="password" name="password" class="form-control" value="qpcheshi678" placeholder="输入 密码">
									</div>
								</div>
							</div>
						</div>

						<div class="fields-group">
							<div class="form-group">
								<label for="otpcode" class="col-sm-2 control-label">OTP code</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="otpcode" name="otpcode" class="form-control" placeholder="输入 OTP code">
									</div>
								</div>
							</div>
						</div>

					</form><!-- .form-horizontal -->

					<hr>

					<form class="form-horizontal">

						<div class="fields-group">
							<div class="form-group">
								<label for="query_state_api" class="col-sm-2 control-label">爬虫状态查询 API</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="query_state_api" name="query_state_api" class="form-control" value="http://192.168.1.246:1048/getSpiderState" placeholder="输入 爬虫状态查询 API">
									</div>
								</div>
							</div>
						</div>

						<div class="fields-group">
							<div class="form-group">
								<label for="spider_last_time" class="col-sm-2 control-label">爬虫最后执行时间</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="spider_last_time" name="spider_last_time" class="form-control" placeholder="爬虫最后执行时间" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="fields-group">
							<div class="form-group">
								<label for="record_last_time" class="col-sm-2 control-label">最后资料时间</label>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
										<input type="text" id="record_last_time" name="record_last_time" class="form-control" placeholder="最后资料时间" readonly>
									</div>
								</div>
							</div>
						</div>

					</form>
				</div><!-- .box-body -->

				<div class="box-footer">
					<div class="col-md-2">
					</div>

					<div class="col-md-8">
						<div class="btn-group pull-left">
							<button type="button" id="query-state-btn" class="btn btn-default">查询爬虫状态</button>
						</div>
						<div class="btn-group pull-right">
							<button type="button" id="send-btn" class="btn btn-primary">爬虫登录</button>
						</div>
					</div>
				</div><!-- .box-footer -->

			</div>
		</div>
	</div>
</section>


<script>
$(function() {
	$("#send-btn").click(function() {
		var $that = $(this),
			f = document.forms[0];

		if ($that.hasClass("disabled")) {
			return;
		}

		if (! confirm("确认?")) {
			return;
		}

		$.ajax({
			url: $("#api_url").val(),
			data: $(f).serialize(),
			type: "get",
			beforeSend: function() {
				$that.addClass("disabled");
			},
			success: function(res) {
				toastr.success("登录成功");
			},
			error: function(jqXHR) {
				alert("登录失败");
			},
			complete: function() {
				$that.removeClass("disabled");
			}
		});
	});


	$("#query-state-btn").click(function() {
		var $that = $(this),
			f = document.forms[1];

		if ($that.hasClass("disabled")) {
			return;
		}

		$.ajax({
			url: $("#query_state_api").val(),
			type: "get",
			dataType: "json",
			beforeSend: function() {
				$that.addClass("disabled");
			},
			success: function(res) {
				toastr.success("查询成功");
				f.spider_last_time.value = res.SpiderLastTime;
				f.record_last_time.value = res.RecordLastTime;
			},
			error: function(jqXHR) {
				alert("查询失败");
			},
			complete: function() {
				$that.removeClass("disabled");
			}
		});
	}).click();
});
</script>
