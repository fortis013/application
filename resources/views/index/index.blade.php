
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css.css?v=0119v2">
    
   <link rel="stylesheet" type="text/css" href="style/top_css.css?v=0119">
	<link rel="stylesheet" type="text/css" href="style/normalize.css">
	<link rel="stylesheet" type="text/css" href="style/notice.css">
	<link rel="stylesheet" type="text/css" href="style/animate.css">
    
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
   
<!--    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>	-->
	<script type="text/javascript" src="js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="js/jquery.cookie.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
<!--	<script type="text/javascript" src="js/time1.js"></script>//測速JS-->
	<script type="text/javascript" src="js/wow.js"></script>

    <title>{{ $setting->title }}</title>

    <script type="text/javascript">
      
        jQuery.fn.float = function (settings) {
            if (typeof settings == "object") {
                settings = jQuery.extend({
                    //延迟
                    delay: 500,
                    //位置偏移
                    offset: {
                        // left : 0,
                        // right : 0,
                        top: 0,
                        bottom: 0
                    },
                    style: null, //样式
                    width: 100,  //宽度
                    height: 1000, //高度
                    position: "rm" //位置
                }, settings || {});
                var winW = $(window).width();
                var winH = $(window).height();

                //根据参数获取位置数值
                function getPosition($applyTo, position) {
                    var _pos = null;
                    switch (position) {
                        case "rm" :
                            $applyTo.data("offset", "right");
                            $applyTo.data("offsetPostion", settings.offset.right);
                            _pos = {right: settings.offset.right, top: winH / 2 - $applyTo.innerHeight() / 2 };
                            break;
                        case "lm" :
                            $applyTo.data("offset", "left");
                            $applyTo.data("offsetPostion", settings.offset.left);
                            _pos = {left: settings.offset.left, top: winH / 2 - $applyTo.innerHeight() / 2 };
                            break;
                        case "rb" :
                            _pos = {right: settings.offset.right, top: winH - $applyTo.innerHeight()};
                            break;
                        case "lb" :
                            _pos = {left: settings.offset.left, top: winH - $applyTo.innerHeight()};
                            break;
                        case "l" :
                            _pos = {left: settings.offset.left, top: settings.offset.top};
                            break;
                        case "r" :
                            _pos = {right: settings.offset.right, top: settings.offset.top};
                            break;
                        case "t" :
                            $applyTo.data("offset", "top");
                            $applyTo.data("offsetPostion", settings.offset.top);
                            _pos = {left: settings.offset.left, top: settings.offset.top};
                            break;
                        case "b" :
                            $applyTo.data("offset", "bottom");
                            $applyTo.data("offsetPostion", settings.offset.bottom);
                            _pos = {left: settings.offset.left, top: winH - $applyTo.innerHeight()};
                            break;
                    }
                    return _pos;
                }

                //设置容器位置
                function setPosition($applyTo, position, isUseAnimate) {
                    var scrollTop = $(window).scrollTop();
                    var scrollLeft = $(window).scrollLeft();
                    var _pos = getPosition($applyTo, position);
                    _pos.top += scrollTop - 35;
                    isUseAnimate && $applyTo.stop().animate(_pos, settings.delay) || $applyTo.css(_pos);
                }

                return this.each(function () {
                    var $this = $(this);
                    $this.css("position", "absolute");
                    settings.style && $this.css(settings.style);
                    setPosition($this, settings.position);
                    $(this).data("isAllowScroll", true);
                    $(window).scroll(function () {
                        $this.data("isAllowScroll") && setPosition($this, settings.position, true);
                    });
                })
            } else {
                var speed = arguments.length > 1 && arguments[1] || "fast";
                this.each(function () {
                    if (settings == "clearOffset") {
                        var _c = {};
                        if ($(this).data("offset")) {
                            _c[$(this).data("offset")] = 0;
                            $(this).data("isAllowScroll", false);
                            $(this).stop().animate(_c, speed);
                        }
                    } else if (settings == "addOffset") {
                        var _c = {};
                        if ($(this).data("offset") && $(this).data("offsetPostion")) {
                            _c[$(this).data("offset")] = $(this).data("offsetPostion");
                            $(this).stop().animate(_c, speed);
                            $(this).data("isAllowScroll", true);
                        }

                    } else if (settings == "setScrollDisable") {
                        $(this).data("isAllowScroll", false);
                    } else if (settings == "setScrollUsable") {
                        $(this).data("isAllowScroll", true);
                    }
                })
            }
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $("#afl").float({
                delay: 1000,//延迟
                position: "lm" //位置
            });
            $("#afr").float({
                delay: 1000,//延迟
                position: "rm" //位置
            });
        });
    </script>
</head>
<body>

 <div class="side_bar" >
<!--
        <div class="af" id="afl">
            <a href="" class="fl1"><img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($leftbutton->img1); ?>" /></a>
            <a href="{{ $leftbutton->link2 }}" {!! $leftbutton->blank2 ? 'target="_blank"' : '' !!} class="fl2"> <img src= "<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($leftbutton->img2); ?>" ></a>
            <a href="{{ $leftbutton->link3 }}" {!! $leftbutton->blank3 ? 'target="_blank"' : '' !!} class="fl3"> <img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($leftbutton->img3); ?>" /></a>
            <a href="{{ $leftbutton->link4 }}" {!! $leftbutton->blank4 ? 'target="_blank"' : '' !!} class="fl4"> <img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($leftbutton->img4); ?>" /></a>
            <a href="{{ $leftbutton->link5 }}" {!! $leftbutton->blank5 ? 'target="_blank"' : '' !!} class="fl5"> <img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($leftbutton->img5); ?>" /></a>
            <span class="fl6">X</span>
        </div>
-->
        <div class="af" id="afr">
<!--
		    <a href="" class="fl1"><img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($rightbutton->img1); ?>
          " /></a>
-->
<!--暫時代打ㄉ浮窗--> <a href="{{ $rightbutton->link2 }}" {!! $rightbutton->blank2 ? 'target="_blank"' : '' !!} class="fl1"><img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($rightbutton->img2); ?>"/></a>
           
           
<!--
            <a href="{{ $rightbutton->link2 }}" {!! $rightbutton->blank2 ? 'target="_blank"' : '' !!} class="fl2"> <img src= "<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($rightbutton->img2); ?>" ></a>
            <a href="{{ $rightbutton->link3 }}" {!! $rightbutton->blank3 ? 'target="_blank"' : '' !!} class="fl3"> <img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($rightbutton->img3); ?>" /></a>
            <a href="{{ $rightbutton->link4 }}" {!! $rightbutton->blank4 ? 'target="_blank"' : '' !!} class="fl4"> <img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($rightbutton->img4); ?>" /></a>
            <a href="{{ $rightbutton->link5 }}" {!! $rightbutton->blank5 ? 'target="_blank"' : '' !!} class="fl5"> <img src="<?php echo \Illuminate\Support\Facades\Storage::disk('admin')->url($rightbutton->img5); ?>" /></a>
-->
            
            <span class="fr6" style="background-image: url(images/X_btn.png)" ></span>
        </div>
    </div>
<!--Header段-->
<div class="top_head"> 
    <div class="jstop">
        <div class="jstop_center clearfix">
            <span class="header-text1">7x24</span>
            <span class="header-text2">小时在线电话客服</span>
            <span class="header-text3">00853-6258-0555</span>
            <div class="right likeul">
                <a id="ch"  href="{{ $top->link1 }}" {!! $top->blank1 ? 'target="_blank"' : '' !!} style="color: rgb(255, 255, 255);">最新优惠</a>|
                <a  href="{{ $top->link2 }}" {!! $top->blank2 ? 'target="_blank"' : '' !!}>升级模式</a>|
                <a id="ch1" href="{{ $top->link3 }}" {!! $top->blank3 ? 'target="_blank"' : '' !!} style="color: rgb(252, 54, 216);">免登录充值中心</a>|
                <a id="ch2" href="{{ $top->link4 }}" {!! $top->blank4 ? 'target="_blank"' : '' !!} style="color: rgb(255, 255, 255);">自助客服</a>|
                <a id="ch3" href="{{ $top->link5 }}" {!! $top->blank5 ? 'target="_blank"' : '' !!} style="color: rgb(252, 54, 216);">代理加盟</a>|
                <a id="ch4" href="{{ $top->link6 }}" {!! $top->blank6 ? 'target="_blank"' : '' !!} style="color: rgb(252, 54, 88);">官网首页</a>|
                <a href="javascript:;" onclick="javascript:try{ window.external.AddFavorite('http://www.www---2004-abc-ddd-cc.com','金沙赌场'); } catch(e){ (window.sidebar)?window.sidebar.addPanel('金沙赌场','http://www.www---2004-abc-ddd-cc.com',''):alert('请点击进入网站后使用按键 Ctrl+d 收藏'); }">加入收藏</a>
            </div>
        </div>
    </div>
    
    <div class="jseondhead">
		<div class="jseondhead_center clearfix">
			<a href="{{ $top->link7 }}" {!! $top->blank7 ? 'target="_blank"' : '' !!} class="jslogo">
				<img src="images/logo.png">
			</a>
			<img src="images/animate.gif" class="myAnim">
			<div class="kf_right"><a  href="{{ $top->link8 }}" {!! $top->blank8 ? 'target="_blank"' : '' !!} class="zxkf"></a></div>		
		</div>
	</div>
  
   <div class="jsnav">
		<ul>
			<li style="background: url(&quot;images/btn01.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link1 }}" {!! $down->blank1 ? 'target="_blank"' : '' !!}>一键入款</a>
			</li>
			<li style="background: url(&quot;images/btn02.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link2 }}" {!! $down->blank2 ? 'target="_blank"' : '' !!}>十一大捕鱼机</a>
			</li>
			<li style="background: url(&quot;images/btn03.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link3 }}" {!! $down->blank3 ? 'target="_blank"' : '' !!}>申请大厅</a>
			</li>
			<li style="background: url(&quot;images/btn04.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link4 }}" {!! $down->blank4 ? 'target="_blank"' : '' !!}>天天红包</a>
			</li> 
			<li style="background: url(&quot;images/btn05.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link5 }}" {!! $down->blank5 ? 'target="_blank"' : '' !!}>手机下注</a>
			</li>
			<li style="background: url(&quot;images/btn06.png&quot;) 0px 0px no-repeat;">
				<a href="{{ $down->link6 }}" {!! $down->blank6 ? 'target="_blank"' : '' !!}>满意度调查</a>
			</li>
			<li style="background: url(&quot;images/btn07.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link7 }}" {!! $down->blank7 ? 'target="_blank"' : '' !!}>资讯端</a>
			</li>
			<li style="background: url(&quot;images/btn08.png&quot;) 0px 0px no-repeat;">
				<a  href="{{ $down->link8 }}" {!! $down->blank8 ? 'target="_blank"' : '' !!}>注册会员</a>
			</li>
		</ul>
	</div>
    
</div>

<div class="yhmain">

    <div class="slider slider1">  
@foreach ($pages as $v)	
        <img src=<?php echo "$v->img"?> data-href=<?php echo "$v->link"?> data-blank=<?php if($v->blank){echo'target="_blank"';}else{echo'';} ?> >
	@endforeach
		<div class="dibubg"></div>
		
    </div>

    <div class="yhmain_center">
 
    <div class="web_name">{{ $setting->name1 }}</div>
        
        <div class="yhconlf">
            <div class="yhgg clearfix" id="yhggContain">
                <span>最新公告：</span>
                <marquee behavior="" direction="">
				
				@foreach ($news as $v)
				
							<a href ="{{ $v->link }}"{!! $v->blank ? 'target="_blank"' : '' !!} style="color:#FFFFFF;">
							<?php echo "$v->content"?>
							</a>
				
				@endforeach
				
                </marquee>
				
            </div>
			
			<table class="tbyh nowyh">
			
                <tr>
                    <th colspan="2">
                        
                        <img src="images/2004103.png">
                        
                    </th>
					
					
                </tr>
				
				
				@foreach ($nowonlys as $k=>$v)
				
					<?php
					
					if($k%2==0)
					{
						echo "<tr>";
					}
					?>

					<td>
                        <?php echo "$v->name"?>
						<a href="{{ $v->link1 }}" class="search" {!! $v->blank1 ? 'target="_blank"' : '' !!}>查看</a>
						<a href="{{ $v->link2 }}" class="aply" {!! $v->blank2 ? 'target="_blank"' : '' !!}>申请</a>
						
                    </td>

					<?php
					if($k%2==1)
					{
						echo "</tr>";
					}
					?>
				
				@endforeach
               <?php
			   $a=count($nowonlys);
			   if($a%2!==0){echo '<td></td></tr>';}
			   ?>
            </table>
			
            <table class="tbyh newyh">
			
                <tr>
                    <th colspan="2">
                        
                        <img src="images/newyh.png">
                        
                    </th>
					
					
                </tr>
				
				
				@foreach ($newsoffer as $k=>$v)
				
					<?php
					
					if($k%2==0)
					{
						echo "<tr>";
					}
					?>

					<td>
                        <?php echo "$v->name"?>
						<a href="{{ $v->link1 }}" class="search" {!! $v->blank1 ? 'target="_blank"' : '' !!}>查看</a>
						<a href="{{ $v->link2 }}" class="aply" {!! $v->blank2 ? 'target="_blank"' : '' !!}>申请</a>
						
                    </td>

					<?php
					if($k%2==1)
					{
						echo "</tr>";
					}
					?>
				
				@endforeach
               <?php
			   $a=count($newsoffer);
			   if($a%2!==0){echo '<td></td></tr>';}
			   ?>
            </table>
            <table class="tbyh eday">
                <tr>
                    <th colspan="2">
                        
                        <img src="images/iday.png">
                        
                    </th>
                </tr>
				
				                
				
				
				@foreach ($daysoffer as $k=>$v)
				
					<?php
					
					if($k%2==0)
					{
						echo "<tr>";
					}
					?>

					<td>
                        <?php echo "$v->name"?>
						<a href="{{ $v->link1 }}" class="search" {!! $v->blank1 ? 'target="_blank"' : '' !!}>查看</a>
						<a href="{{ $v->link2 }}" class="aply" {!! $v->blank2 ? 'target="_blank"' : '' !!}>申请</a>
						
                    </td>

					<?php
					if($k%2==1)
					{
						echo "</tr>";
					}
					?>
				
				@endforeach
               <?php
			   $a=count($daysoffer);
			   if($a%2!==0){echo '<td></td></tr>';}
			   ?>                
            </table>

            <table class="tbyh  eweek">
                <tr>
                    <th colspan="2">
                        <img src="images/iweek.png">
                    </th>
                </tr>
				                
				
				
				@foreach ($weeksoffer as $k=>$v)
				
					<?php
					
					if($k%2==0)
					{
						echo "<tr>";
					}
					?>

					<td>
                        <?php echo "$v->name"?>
						<a href="{{ $v->link1 }}" class="search" {!! $v->blank1 ? 'target="_blank"' : '' !!}>查看</a>
						<a href="{{ $v->link2 }}" class="aply" {!! $v->blank2 ? 'target="_blank"' : '' !!}>申请</a>
						
                    </td>

					<?php
					if($k%2==1)
					{
						echo "</tr>";
					}
					?>
				
				@endforeach
               <?php
			   $a=count($weeksoffer);
			   if($a%2!==0){echo '<td></td></tr>';}
			   ?> 
            </table>

            <table class="tbyh  emonth">
                <tr>
                    <th colspan="2">
                        
                        <img src="images/imouth.png">
                        
                    </th>
                </tr>
				@foreach ($monthsoffer as $k=>$v)
				
					<?php
					
					if($k%2==0)
					{
						echo "<tr>";
					}
					?>

					<td>
                        <?php echo "$v->name"?>
						<a href="{{ $v->link1 }}" class="search" {!! $v->blank1 ? 'target="_blank"' : '' !!}>查看</a>
						<a href="{{ $v->link2 }}" class="aply" {!! $v->blank2 ? 'target="_blank"' : '' !!}>申请</a>
						
                    </td>

					<?php
					if($k%2==1)
					{
						echo "</tr>";
					}
					?>
				
				@endforeach
               <?php
			   $a=count($monthsoffer);
			   if($a%2!==0){echo '<td></td></tr>';}
			   ?> 
            </table>

            <table class="tbyh eyear">
                <tr>
                    <th colspan="2">
                       
                        <img src="images/iyear.png">
                        
                    </th>
                </tr>
				@foreach ($yearsoffer as $k=>$v)
				
					<?php
					
					if($k%2==0)
					{
						echo "<tr>";
					}
					?>

					<td>
                        <?php echo "$v->name"?>
						<a href="{{ $v->link1 }}" class="search" {!! $v->blank1 ? 'target="_blank"' : '' !!}>查看</a>
						<a href="{{ $v->link2 }}" class="aply" {!! $v->blank2 ? 'target="_blank"' : '' !!}>申请</a>
						
                    </td>

					<?php
					if($k%2==1)
					{
						echo "</tr>";
					}
					?>
				
				@endforeach
               <?php
			   $a=count($yearsoffer);
			   if($a%2!==0){echo '<td></td></tr>';}
			   ?> 
            </table>
            <div class="yhfoot" style="letter-spacing: ">Copyright ©  金沙赌场 Reserved</div>
        </div>
    </div>

</div>

<!--彈窗-->
<!--
<div id="js-notice-mask-wrap" class="ele-notice-mask-wrap" style="width: 1403px; ">
    <div id="js-notice-pop" style="top: 177px; left: 162px;">
        <h1 class="ele-notice-title">
            平台公告
            <i id="js-noticle-close" class="fa fa-times"> X </i>
        </h1>
        <div id="ele-notice-inner" class="ele-notice-inner clearfix">
            <div id="ele-notice-sidemenu">
                <ul>
								
					<li>
                        <div id="js-notice-menu-0" class="ele-notice-menu ele-notice-menu-0 ele-notice-menu-active"
                             title="★开元棋牌—乐享棋成：53262AA.COM"><span class="ele-notice-icon"></span>★开元棋牌—乐享棋成：53262AA.COM
                           
                        </div>
                        <div id="js-notice-mobile-content-0" class="ele-notice-mobile-content">
                            <div id="js-notice-subtitle">★开元棋牌—乐享棋成：53262AA.COM</div>
                            <img src="./images/tan1.jpg">
                        </div>
                    </li>
								
                   
                    <li>
                        <div id="js-notice-menu-1" class="ele-notice-menu ele-notice-menu-1 "
                             title="★寰宇浏览器手机 电脑：53262CC.COM"><span class="ele-notice-icon"></span>★寰宇浏览器手机
                            电脑：53262CC.COM
                        </div>
                        <div id="js-notice-mobile-content-1" class="ele-notice-mobile-content">
                            <div id="js-notice-subtitle">★寰宇浏览器手机 电脑：53262CC.COM</div>
                            <img src="./images/153069831240.jpg">
                        </div>
                    </li>
                    <li>
                        <div id="js-notice-menu-2" class="ele-notice-menu ele-notice-menu-2 "
                             title="★网址打不开 立即解决：53262DD.COM"><span class="ele-notice-icon"></span>★网址打不开
                            立即解决：53262DD.COM
                        </div>
                        <div id="js-notice-mobile-content-2" class="ele-notice-mobile-content">
                            <div id="js-notice-subtitle">★网址打不开 立即解决：53262DD.COM</div>
                            <img src="./images/152566191993.jpg">
                        </div>
                    </li>
                    <li>
                        <div id="js-notice-menu-3" class="ele-notice-menu ele-notice-menu-3 "
                             title="★客服中心 24小时在线：53262DD.COM"><span class="ele-notice-icon"></span>★客服中心
                            24小时在线：53262DD.COM
                        </div>
                        <div id="js-notice-mobile-content-3" class="ele-notice-mobile-content">
                            <div id="js-notice-subtitle">★客服中心 24小时在线：53262DD.COM</div>
                            <div class="ele-notice-content-style">
                                <table style="width:670px" cellspacing="0" cellpadding="0" border="1">
                                    <tbody>
                                    <tr>
                                        <td colspan="5" style="height:30px; text-align:center"><strong><span
                                                style="font-size:16pt"><span
                                                style="color:rgb(255, 0, 0)">ᴥ✿</span></span></strong><span
                                                style="font-size:16px"><a
                                                href="https://ub.xf0371.com/UB/UB-Launcher.exe" target="_blank"><span
                                                style="color:#FF0000"><strong><span style="background-color:#FFFF00">寰宇浏览器IOS版、Android版同步推出 高效、安全、快速，自动选择最佳线路</span></strong></span></a></span><strong><span
                                                style="font-size:16pt"><span
                                                style="color:rgb(255, 0, 0)">✿ᴥ</span></span></strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" style="height:30px"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">&nbsp;常见问题咨询：<a
                                                href="javascript:ckEditorLink('service');" target="_blank"><span
                                                style="color:rgb(0, 0, 255)">7*24小时在线客服</span></a>，或者通过以下方式联系我们：</span></span></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong><span
                                                style="color:rgb(255, 0, 0)">❤</span>QQ</strong></span></span><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">客服</span></span></strong><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>：<span style="color:rgb(255, 0, 0)">3358888888</span></strong></span></span>
                                        </td>
                                        <td colspan="2" style="height:30px"><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong><span
                                                style="color:rgb(255, 0, 0)">❤</span></strong></span></span><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">微信客服</span></span></strong><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>：<span style="color:rgb(255, 0, 0)">juezhan1897</span></strong></span></span>
                                        </td>
                                        <td colspan="2"><span style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong><span
                                                style="color:rgb(255, 0, 0)">❤</span></strong></span></span><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">澳门热线</span></span></strong><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>：<span style="color:rgb(255, 0, 0)">00853-62526999</span></strong></span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"><p style="margin-left:80px"><strong><span
                                                style="font-size:22px"><span
                                                style="color:rgb(255, 0, 0)">༺༺༺</span></span><span
                                                style="font-size:16pt"><span
                                                style="color:rgb(255, 0, 0)">ᴥ✿</span></span><span
                                                style="color:#FF0000"><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px"><span style="background-color:#FFFF00">澳门巴黎人娱乐城，服务至上，专注细节！</span></span></span></span><span
                                                style="font-size:16pt"><span
                                                style="color:rgb(255, 0, 0)">✿ᴥ༻༻༻</span></span></strong></p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:30px"><span
                                                style="color:rgb(255, 0, 0); font-family:sans-serif,arial,verdana,trebuchet ms"><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>❤</strong></span></span></span><span
                                                style="color:#000000"><span
                                                style="font-family:sans-serif,arial,verdana,trebuchet ms"><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>存款未到账</strong></span></span></span></span><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">QQ</span></span></strong><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>：1444646339</strong></span></span></td>
                                        <td colspan="2"><span
                                                style="color:rgb(255, 0, 0); font-family:sans-serif,arial,verdana,trebuchet ms; font-size:13px"><span
                                                style="font-family:arial,helvetica,sans-serif"><span
                                                style="font-size:16px"><strong>❤</strong></span></span></span><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">投诉QQ</span></span></strong><span
                                                style="color:rgb(51, 51, 51); font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                                style="font-size:16px"><strong>：</strong></span></span><span
                                                style="font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                                style="font-size:16px"><strong>1113335367</strong></span></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"><span style="font-size:14px"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial">添加QQ时请根据页面的验证消息，先写会员账号以及投诉内容，澳门巴黎人娱乐城专员投诉平台竭诚为您服务！</span></strong></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:30px"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px">活动专员邮箱</span></span></strong><span
                                                style="font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                                style="font-size:16px"><strong>：aomenblr888@gmail.com​</strong></span></span>
                                        </td>
                                        <td colspan="2"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px"><span
                                                style="color:#FF0000">❤</span>代理QQ</span></span></strong><span
                                                style="font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                                style="font-size:16px"><strong>：2221119367</strong></span></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"><p style="margin-left:80px"><strong><span
                                                style="font-size:22px"><span
                                                style="color:rgb(255, 0, 0)">༺</span></span><span
                                                style="font-size:16pt"><span
                                                style="color:rgb(255, 0, 0)">ᴥ✿</span></span></strong><span
                                                style="background-color:rgb(255, 255, 255); color:rgb(255, 0, 0); font-family:sans-serif,arial,verdana,trebuchet ms; font-size:13px"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px"><span style="background-color:rgb(255, 255, 0)">创新理念、奢华服务、高端平台、多元化游戏，顶级线上娱乐城</span></span></span></strong></span><strong><span
                                                style="font-size:16pt"><span
                                                style="color:rgb(255, 0, 0)">✿ᴥ༻</span></span></strong></p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" style="height:60px"><p style="margin-left:40px"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px"><a href="https://5556367.com/infe/rmobile"
                                                                          target="_blank"><span
                                                style="color:#FF0000"><span style="background-color:#FFFF00">点击立即下载澳门巴黎人专属APP，</span></span></a><span
                                                style="color:#0000FF">精彩、唯一，58888元彩金触手可及！</span></span></span></strong>
                                        </p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"><p style="margin-left:80px"><strong><span
                                                style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                                style="font-size:16px"><span style="color:#FF0000">专属防劫持</span>，添加网址后缀【：8888：9988：8859：8899：9885】<br>简单快捷 轻松随意 我司任意一条主网址添加后缀即可登录，例如：</span></span></strong>
                                        </p>
                                            <p style="margin-left:200px"><strong><span
                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                    style="font-size:16px"><a href="http://801252.com:8888/"
                                                                              an
                                                    style="color:rgb(255, 0, 0)">:8888</span></a><br><a
                                               
                                                    href="http://801303.com:8859/"
                                                    target="_blank">http://801303.com<span style="color:rgb(255, 0, 0)">:8859</span></a></span></span></strong>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>					
                </ul>
            </div>
            <div id="ele-notice-content-wrap" class="ele-notice-content-wrap">					
				<div id="js-notice-content-0" class="ele-notice-content ele-notice-content-current">
                    <div id="js-notice-subtitle">★开元棋牌—乐享棋成：53262AA.COM</div>
                    <img src="./images/tan1.jpg">
                </div> 				
                <div id="js-notice-content-1" class="ele-notice-content ">
                    <div id="js-notice-subtitle">★寰宇浏览器手机 电脑：53262CC.COM</div>
                    <img src="./images/153069831240.jpg">
                </div>
                <div id="js-notice-content-2" class="ele-notice-content ">
                    <div id="js-notice-subtitle">★网址打不开 立即解决：53262DD.COM</div>
                    <img src="./images/152566191993.jpg">
                </div>
                <div id="js-notice-content-3" class="ele-notice-content ">
                    <div id="js-notice-subtitle">★客服中心 24小时在线：53262DD.COM</div>
                    <div class="ele-notice-content-style">
                        <table style="width:670px" cellspacing="0" cellpadding="0" border="1">
                            <tbody>
                            <tr>
                                <td colspan="5" style="height:30px; text-align:center"><strong><span
                                        style="font-size:16pt"><span
                                        style="color:rgb(255, 0, 0)">ᴥ✿</span></span></strong><span
                                        style="font-size:16px"><a href="https://ub.xf0371.com/UB/UB-Launcher.exe"
                                                                  target="_blank"><span
                                        style="color:#FF0000"><strong><span style="background-color:#FFFF00">寰宇浏览器IOS版、Android版同步推出 高效、安全、快速，自动选择最佳线路</span></strong></span></a></span><strong><span
                                        style="font-size:16pt"><span
                                        style="color:rgb(255, 0, 0)">✿ᴥ</span></span></strong></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="height:30px"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">&nbsp;常见问题咨询：<a
                                        href="javascript:ckEditorLink('service');" target="_blank"><span
                                        style="color:rgb(0, 0, 255)">7*24小时在线客服</span></a>，或者通过以下方式联系我们：</span></span></strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong><span
                                        style="color:rgb(255, 0, 0)">❤</span>QQ</strong></span></span><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">客服</span></span></strong><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>：<span
                                        style="color:rgb(255, 0, 0)">3358888888</span></strong></span></span></td>
                                <td colspan="2" style="height:30px"><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong><span
                                        style="color:rgb(255, 0, 0)">❤</span></strong></span></span><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">微信客服</span></span></strong><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>：<span
                                        style="color:rgb(255, 0, 0)">juezhan1897</span></strong></span></span></td>
                                <td colspan="2"><span style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong><span
                                        style="color:rgb(255, 0, 0)">❤</span></strong></span></span><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">澳门热线</span></span></strong><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>：<span style="color:rgb(255, 0, 0)">00853-62526999</span></strong></span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><p style="margin-left:80px"><strong><span style="font-size:22px"><span
                                        style="color:rgb(255, 0, 0)">༺༺༺</span></span><span style="font-size:16pt"><span
                                        style="color:rgb(255, 0, 0)">ᴥ✿</span></span><span style="color:#FF0000"><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px"><span style="background-color:#FFFF00">澳门巴黎人娱乐城，服务至上，专注细节！</span></span></span></span><span
                                        style="font-size:16pt"><span
                                        style="color:rgb(255, 0, 0)">✿ᴥ༻༻༻</span></span></strong></p></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="height:30px"><span
                                        style="color:rgb(255, 0, 0); font-family:sans-serif,arial,verdana,trebuchet ms"><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>❤</strong></span></span></span><span
                                        style="color:#000000"><span
                                        style="font-family:sans-serif,arial,verdana,trebuchet ms"><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>存款未到账</strong></span></span></span></span><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">QQ</span></span></strong><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>：1444646339</strong></span></span></td>
                                <td colspan="2"><span
                                        style="color:rgb(255, 0, 0); font-family:sans-serif,arial,verdana,trebuchet ms; font-size:13px"><span
                                        style="font-family:arial,helvetica,sans-serif"><span
                                        style="font-size:16px"><strong>❤</strong></span></span></span><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">投诉QQ</span></span></strong><span
                                        style="color:rgb(51, 51, 51); font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                        style="font-size:16px"><strong>：</strong></span></span><span
                                        style="font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                        style="font-size:16px"><strong>1113335367</strong></span></span></td>
                            </tr>
                            <tr>
                                <td colspan="5"><span style="font-size:14px"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial">添加QQ时请根据页面的验证消息，先写会员账号以及投诉内容，澳门巴黎人娱乐城专员投诉平台竭诚为您服务！</span></strong></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="height:30px"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px">活动专员邮箱</span></span></strong><span
                                        style="font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                        style="font-size:16px"><strong>：aomenblr888@gmail.com​</strong></span></span>
                                </td>
                                <td colspan="2"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px"><span
                                        style="color:#FF0000">❤</span>代理QQ</span></span></strong><span
                                        style="font-family:arial,helvetica,sans-serif; font-size:13px"><span
                                        style="font-size:16px"><strong>：2221119367</strong></span></span></td>
                            </tr>
                            <tr>
                                <td colspan="5"><p style="margin-left:80px"><strong><span style="font-size:22px"><span
                                        style="color:rgb(255, 0, 0)">༺</span></span><span style="font-size:16pt"><span
                                        style="color:rgb(255, 0, 0)">ᴥ✿</span></span></strong><span
                                        style="background-color:rgb(255, 255, 255); color:rgb(255, 0, 0); font-family:sans-serif,arial,verdana,trebuchet ms; font-size:13px"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px"><span style="background-color:rgb(255, 255, 0)">创新理念、奢华服务、高端平台、多元化游戏，顶级线上娱乐城</span></span></span></strong></span><strong><span
                                        style="font-size:16pt"><span
                                        style="color:rgb(255, 0, 0)">✿ᴥ༻</span></span></strong></p></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="height:60px"><p style="margin-left:40px"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px"><a href="https://5556367.com/infe/rmobile"
                                                                  target="_blank"><span style="color:#FF0000"><span
                                        style="background-color:#FFFF00">点击立即下载澳门巴黎人专属APP，</span></span></a><span
                                        style="color:#0000FF">精彩、唯一，58888元彩金触手可及！</span></span></span></strong></p></td>
                            </tr>
                            <tr>
                                <td colspan="5"><p style="margin-left:80px"><strong><span
                                        style="font-family:stheiti,microsoft yahei,微软雅黑,simsun,宋体,arial; font-size:12px"><span
                                        style="font-size:16px"><span style="color:#FF0000">专属防劫持</span>，添加网址后缀【：8888：9988：8859：8899：9885】<br>简单快捷 轻松随意 我司任意一条主网址添加后缀即可登录，例如：</span></span></strong>
                                </p>
                                    <p style="margin-left:200px"><strong><span
                                            style="font-family:arial,helvetica,sans-serif"><span style="font-size:16px"><a
                                            href="http://801252.com:8888/" target="_blank">http://801252.com<span
                                            style="color:rgb(255, 0, 0)">:8888</span></a><br><a
                                           
                                          
                                            href="http://801303.com:8859/" target="_blank">http://801303.com<span
                                            style="color:rgb(255, 0, 0)">:8859</span></a></span></span></strong></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>				
            </div>
        </div>
    </div>
</div>
-->

<script>
    (function () {
        var notice = $('#js-notice-pop'),
            // recurrence = 'once',
            contentWidth = parseInt(700 + 30),
            noticeWidth = contentWidth,
            setting,
            positionX,
            positionY;

        if ($.cookie('NP_8600')) {
            $('#js-notice-mask-wrap').hide();
            console.log(2);
        }else{
            $.cookie('NP_8600', 's');
            console.log(1);
            $('#js-notice-mask-wrap').css({width: $('body').width(), height: $('body').height()}).fadeIn(3000);
            $('#js-notice-mask-wrap').height($('body').height()).fadeIn(300);
        }

        // if (recurrence === 'once') {
        //     $.cookie('NP_860005', 'Y', {path:'/', expires: ''});
        // }

        // $('input[name="notremind"]').click(function() {
        //     if ($(this).is(':checked')) {
        //         $.cookie('NP_860005', 'Y', {path:'/', expires: ''});
        //         return;
        //     }
        //     $.cookie('NP_860005', '', {path:'/', expires: -1});
        // });



        if ($('#ele-notice-sidemenu').length === 1) {
            noticeWidth = parseInt(contentWidth + 348);
        }

        /* 彈跳視窗設定 */
        positionX = Math.floor(($(window).width() - noticeWidth) / 2),
            positionY = Math.floor(($(window).height() - notice.height()) / 2);

        if (positionY < 0) {
            positionY = 0;
        }

        setting = {
            'top': positionY,
            'left': positionX
        };

        if (window.innerWidth <= noticeWidth) {
            setting = {
                'top': positionY,
                'left': 0
            };
        }

        notice.css(setting);

        $('#js-noticle-close').click(function () {
            $('#js-notice-mask-wrap').fadeOut();
        });

        $('#ele-notice-sidemenu li').click(function () {
            var menuIndex = $(this).index();

            $('.ele-notice-menu').removeClass('ele-notice-menu-active')
                .eq(menuIndex)
                .addClass('ele-notice-menu-active');

            /* 電腦版 - 內容區塊切換 */
            $('.ele-notice-content').removeClass('ele-notice-content-current')
                .eq(menuIndex)
                .addClass('ele-notice-content-current');

            /* 行動裝置 - 內容區塊切換 */
            if (window.innerWidth <= noticeWidth) {
                if ($(this).find('.ele-notice-content-current').is(':visible')) {
                    return;
                }
                $('.ele-notice-mobile-content').slideUp();
                $(this).find('.ele-notice-mobile-content').addClass('ele-notice-content-current').slideDown();

                $('html, body').delay('200').animate({
                    scrollTop: 63 + (50 * $(this).index())
                }, 200);
            }
        });

        $.each($('.js-notice-pop-swf'), function (index) {
            var target = $(this).attr('id'),
                filepath = $(this).data('path');

            swfobject.embedSWF(filepath, target, "100%", "100%", '9.0.0', "/cl/tpl/commonFile/swf/expressInstall.swf", {}, {
                wmode: 'transparent',
                quality: 'high'
            }, {});
        });

        $(window).resize(function () {
            if ($('#js-notice-mask-wrap').is(':visible')) {
                positionX = Math.floor(($(window).width() - noticeWidth) / 2),
                    positionY = Math.floor(($(window).height() - notice.height()) / 2);

                if (positionY < 0) {
                    positionY = 0;
                }

                $('#js-notice-mask-wrap')
                    .css({
                        width: $('body').width(),
                        height: $('body').height()
                    });

                if (window.innerWidth <= noticeWidth) {
                    notice.css({
                        'top': positionY,
                        'left': 0
                    });
                    $('.ele-notice-menu-active').next('.ele-notice-mobile-content').addClass('ele-notice-content-current');
                    $('.ele-notice-menu-active').next('.ele-notice-mobile-content').slideDown();
                    return;
                }

                notice.css({
                    'top': positionY,
                    'left': positionX
                });
                $('.ele-notice-mobile-content').removeClass('ele-notice-content-current');
                $('.ele-notice-mobile-content').slideUp();
            }
        });
    })();
</script>



<!--<script src="js/adress.js?v=1.656"></script>-->
<script src="js/jquery.easing.min.js"></script>
<script type="text/javascript" src="js/jquery.bscslider.js"></script>
</body>
</html>
<script type="text/javascript">
    document.ondragstart = function () {
        return false;
    };
    $('.slider1').bscSlider({
        duration: 3000,
        effect: 1,
        navigation: true,
        effect_speed: 750,
        easing: 'easeOutQuad',
        height: 624,
    });
    
    
    $('span.fl6').click(function () {
        $('#afl').hide();
        console.log('leftX');
    })
    $('span.fr6').click(function () {
        $('#afr').hide();
        console.log('rightX');
    })
</script>
<script type="text/javascript">
	//实现网站颜色跳动
	    var n=true;
		var jj = true;
	    function start(){
	        if(n){
	            $(".jslot i").css("background","url(images/light-01.png)");	           
	            n=false;
	        }else{
	            $(".jslot i").css("background","url(images/light-02.png)"); 
	            n=true;
	        }
	    }
		function startjc(){
			if(jj){
				$(".modewb:even span").css("color","#0878C7");
				$(".modewb:odd span").css("color","#0d9642");
				jj=false;
			}else{
				$(".modewb:even span").css("color","#0d9642");
				$(".modewb:odd span").css("color","#0878C7");
				jj=true;
			}
		}
		
	$(function(){
		$('.modewb').each(function(){
			var txt = $(this).clone().html();
			$(this).html('');
			var str1_num = txt.split(':');			
			var span_all = str1_num[1].substr(5,4);
			var aspan = str1_num[1].substr(0,5);
			$(this).html(str1_num[0]+':'+aspan+'<span>'+span_all+'</span>.com');
		});		
		
		
		
		//时分秒
//		GetTime();
//		setInterval(GetTime,0);
//		 
//	    start();
//	    setInterval(start,500);
//		
//		startjc();
//		setInterval(startjc,500);


		$('.jsnav ul li').each(function(){
			var index = $(this).index()+1;
			$(this).css('background','url(images/btn0'+index+'.png) no-repeat 0 0');
			$(this).hover(function(){
				$(this).css('background','url(images/btn0'+index+'.png) no-repeat 0 -50px');
			},function(){
			$(this).css('background','url(images/btn0'+index+'.png) no-repeat 0 0');
			});
		});
		//选项卡开始
		$('.conyyhui').eq(0).show();
		$('.newul li').click(function(){
			$('.newul li').removeClass('active');
			$(this).addClass('active');
			var ind = $(this).index();
			$('.conyyhui').eq(ind).show().siblings().hide();
		});
		//选项卡结束


		//轮播开始
		$(".main_visual").hover(function(){
			$("#btn_prev,#btn_next").fadeIn()
		},function(){
				$("#btn_prev,#btn_next").fadeOut()
		});
			
			$dragBln = false;			
			$(".main_image").touchSlider({
				flexible : true,
				speed : 200,
				btn_prev : $("#btn_prev"),
				btn_next : $("#btn_next"),
				paging : $(".flicking_con a"),
				counter : function (e){
					$(".flicking_con a").removeClass("on").eq(e.current-1).addClass("on");
				}
			});
			
			$(".main_image").bind("mousedown", function() {
				$dragBln = false;
			});
			
			$(".main_image").bind("dragstart", function() {
				$dragBln = true;
			});
	
			$(".main_image a").click(function(){
				if($dragBln) {
					return false;
				}
			});
			
			timer = setInterval(function(){
				$("#btn_next").click();
			}, 5000);
			
			$(".main_visual").hover(function(){
				clearInterval(timer);
			},function(){
				timer = setInterval(function(){
					$("#btn_next").click();
				},5000);
			});
			
			$(".main_image").bind("touchstart",function(){
				clearInterval(timer);
			}).bind("touchend", function(){
				timer = setInterval(function(){
					$("#btn_next").click();
				}, 5000);
			});
		//轮播结束

		$('.fix_close').click(function(){			
		    $(this).parent('.fixbottom').hide();
		    scl=2;		 
		});	

	})
	
$(function(){
				
console.log('高'+$(document).height());
console.log('寬'+$(document).width());

        var notice = $('#js-notice-pop'),
            recurrence = 'once',
            noticeContent = $('#ele-notice-content-wrap'),
            contentWidth = parseInt(730 + 30),
            noticeWidth = contentWidth,
            setting,
            positionX,
            positionY;

		/*
        if ($.cookie('NP_18676') === 'Y') {
            return
        }

        if (recurrence === 'once') {
            $.cookie('NP_18676', 'Y', {path:'/', expires: ''});
        }

        $('input[name="notremind"]').click(function() {
            if ($(this).is(':checked')) {
                $.cookie('NP_18676', 'Y', {path:'/', expires: ''});
                return;
            }
            $.cookie('NP_18676', '', {path:'/', expires: -1});
        });
		*/

        $('#js-notice-mask-wrap')
            .css({
                width: $('body').width(),
                height:$(document).height()
            })
            .fadeIn(300);

        $('#js-notice-mask-wrap').height($('body').height()).fadeIn(300);

        if ($('#ele-notice-sidemenu').length === 1) {
            noticeWidth = parseInt(contentWidth + 348);
        }

        /* 彈跳視窗設定 */
        positionX = Math.floor(($(window).width() - noticeWidth)/2),
        positionY = Math.floor(($(window).height() - notice.height())/2);

        if (positionY < 0) {
            positionY = 0;
        }

        setting = {
            'top': positionY,
            'left': positionX
        };

        if (window.innerWidth <= noticeWidth){
            setting = {
                'top': positionY,
                'left': 0
            };
        }

        notice.css(setting);

        $('#js-noticle-close').click(function(){
            $('#js-notice-mask-wrap').fadeOut();
        });

        $('#ele-notice-sidemenu li').click(function(){
            var menuIndex = $(this).index();
            
            $('.ele-notice-menu').removeClass('ele-notice-menu-active')
                .eq(menuIndex)
                .addClass('ele-notice-menu-active');

            /* 電腦版 - 內容區塊切換 */
            $('.ele-notice-content').removeClass('ele-notice-content-current')
                .eq(menuIndex)
                .addClass('ele-notice-content-current');

            /* 行動裝置 - 內容區塊切換 */
            if (window.innerWidth <= noticeWidth) {
                if ($(this).find('.ele-notice-content-current').is(':visible')) {
                    return;
                }
                $('.ele-notice-mobile-content').slideUp();
                $(this).find('.ele-notice-mobile-content').addClass('ele-notice-content-current').slideDown();

                $('html, body').delay('200').animate({
                    scrollTop: 63 + (50 * $(this).index())
                }, 200);
            }
        });

        $(window).resize(function(){
            if ($('#js-notice-mask-wrap').is(':visible')) {
                positionX = Math.floor(($(window).width() - noticeWidth)/2),
                positionY = Math.floor(($(window).height() - notice.height())/2);

                if (positionY < 0) {
                    positionY = 0;
                }

                $('#js-notice-mask-wrap')
                    .css({
                        width: $('body').width(),
                        height: $(document).height()
                    });

                if (window.innerWidth <= noticeWidth) {
                    notice.css({
                        'top': positionY,
                        'left': 0
                    });
                    $('.ele-notice-menu-active').next('.ele-notice-mobile-content').addClass('ele-notice-content-current');
                    $('.ele-notice-menu-active').next('.ele-notice-mobile-content').slideDown();
                    return;
                }

                notice.css({
                    'top': positionY,
                    'left': positionX
                });
                $('.ele-notice-mobile-content').removeClass('ele-notice-content-current');
                $('.ele-notice-mobile-content').slideUp();
            }
        });
    });	
	
</script>
<script type="text/javascript">
	//odoo.default({ el:'.js-odoo',value:'967224583.07' })
	
//	setInterval(function(){
//		isFirst = false;
//		_from = $('#count-number').data('to');
//		_data_to = parseFloat(_from) + parseFloat(66.47);
//		$('#count-number').data('from',_from).data('to',_data_to);
//		$(".timer").countTo();
//	}, 5000);
	
	//大赢家	
	//jQuery(".conyyhui").slide({mainCell:".inbd ul",autoPlay:true,effect:"topMarquee",vis:7,interTime:50});
</script>
<script>
        wow = new WOW(
            {
                animateClass: 'animated',
                offset: 200,
                callback: function(box) {
                    console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
                }
            }
        );
        wow.init();
			setInterval(function(){
				$(document).height('2836')
//		isFirst = false;
//		_from = $('#count-number').data('to');
//		_data_to = parseFloat(_from) + parseFloat(66.47);
//		$('#count-number').data('from',_from).data('to',_data_to);
//		$(".timer").countTo();
	}, 3000);
	
		//$(document).height('2836');
    </script>