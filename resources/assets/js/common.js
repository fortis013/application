	jQuery.zabsuper = {
		buttonEffect : function(option) { 
			var defaults = {    
			   theme: 'default' ,
				bgColor:{flag:false,from:'#000',to:'#f60'},
				fontColor:{flag:false,from:'#f60',to:'#fff'},
				duration:500,
				easing:'easeOutQuint'
			};   
			var opts = $.extend(defaults, option); 
			if(!opts.selector)return false;
			$(opts.selector).each(function(){
				$(opts.selector).addClass('myeffect');
				var that = $(this);
				var thisH = $(this).height();
				var htmls = $(this).html();
				$(this).html('');
				var effectbox=$('<div class=effectbox style="width:100%;height:100%;position:relative;overflow:hidden;"></div>');
				effectbox.addClass(opts.theme).append('<div class=up style="width:100%;height:100%;position:absolute;top:-'+thisH+'px;">'+htmls+'</div><div class=down style="width:100%;height:100%;position:absolute;top:0px;">'+htmls+'</div>');
				effectbox.appendTo(this);
				if(opts.bgColor.flag){
					effectbox.css('background-color',opts.bgColor.from);
				}
				if(opts.fontColor.flag){
					effectbox.find('.up>*').css('color',opts.fontColor.to);
					effectbox.find('.down>*').css('color',opts.fontColor.from);
				}

				effectbox.hover(function(){
					if(opts.bgColor.flag){
						$(this).stop(true,false).animate({backgroundColor:opts.bgColor.to},opts.duration);
					}
					if(opts.fontColor.flag){
						$(this).find('.up>*').stop(true,false).animate({color:opts.fontColor.to},opts.duration,opts.easing);
						$(this).find('.down>*').stop(true,false).animate({color:opts.fontColor.to},opts.duration,opts.easing);
					}
					$(this).find('.up').stop(true,false).animate({top:'0px',opacity:1},opts.duration,opts.easing);
					$(this).find('.down').stop(true,false).animate({top:thisH,opacity:0},opts.duration,opts.easing);
					
				},function(){
					if(opts.bgColor.flag){
						$(this).stop(true,false).animate({backgroundColor:opts.bgColor.from},opts.duration);
					}
					if(opts.fontColor.flag){
						$(this).find('.up>*').stop(true,false).animate({color:opts.fontColor.from},opts.duration,opts.easing);
						$(this).find('.down>*').stop(true,false).animate({color:opts.fontColor.from},opts.duration,opts.easing);
					}
					$(this).find('.up').stop(true,false).animate({top:-thisH,opacity:0},opts.duration,opts.easing);
					$(this).find('.down').stop(true,false).animate({top:'0px',opacity:1},opts.duration,opts.easing);
					
				});
			});
		}	
	};
	
    $(function(){
        //$('.login .js').focus(function(){
//            var txt_value = $(this).val();
//            if(txt_value == this.defaultValue){$(this).val('');}
//        }).blur(function(){
//            var txt_value = $(this).val();
//            if(txt_value == ''){$(this).val(this.defaultValue);}
//        });

       // $("#showPwd").focus(function() {  
//            var text_value = $(this).val();  
//            if (text_value == this.defaultValue) {$("#showPwd").hide(); $("#password").show().focus();}  
//        });  
//        $("#password").blur(function() {  
//            var text_value = $(this).val();  
//            if (text_value == "") {$("#showPwd").show();$("#password").hide();}  
//        }); 

		var $el, leftPos,$move = $("#headerbg .nav .move");
	    //$move.css("left", $(".nav .on").position().left - 4).data("origLeft", $(".nav .on").position().left);   
 		if(!$("#headerbg .nav .on")[0]){
 			$move.css("left", '-120px').data("origLeft", -120);
 	 	}else{
	 		 $move.css("left", $("#headerbg .nav .on").position().left-4).data("origLeft", $("#headerbg .nav .on").position().left); 
 	 	}
	    $("#headerbg .nav li a").hover(function() {
	        $el = $(this);
	        leftPos = $el.parent().position().left;
	        $move.stop(true,false).animate({
	            left: leftPos-4
	        },{duration:"200"});
	    }, function() {
	        $move.stop(true,false).animate({
	            left: $move.data("origLeft") -4
	        },{duration:"200"});   
	    });

	    //$.zabsuper.buttonEffect({
//			selector:'.submit'
//		});
//
//	    $.zabsuper.buttonEffect({
//			selector:'.reg'
//		});
		$.zabsuper.buttonEffect({
			selector:'.online'
		});

		$.zabsuper.buttonEffect({
			selector:'.button2',
			bgColor:{flag:true,from:'#aa6f3c',to:'#ffae00'}
		});
		$.zabsuper.buttonEffect({
			selector:'.button1',
			bgColor:{flag:true,from:'#533a27',to:'#f60'}
		});
		$.zabsuper.buttonEffect({
			selector:'.button3'
		});

		
		//progressbar 
	    if($(".progress")[0]){progressbar([18,138,35],1000); }
	    
	    $(window).bind('scroll',goscroll);
		$(window).bind('resize',goscroll);
		function goscroll(){
			//alert(1);
			setTimeout(function(){
				$('#headerbg').each(function(){
					var scroH = $(window).scrollTop();
					if(scroH>107){
						$(this).css({'position':'fixed','top':'-107px','z-index':10000});
					}else {
						$(this).css({'position':'absolute','top':'0px'});
					}
				});
			},50);
		}
    
   });
   
   
   function progressbar(arr,delay){
	var divL1 = $('.m_in .progress')[0];
	var divL2 = $('.m_out .progress')[0];
	var s1 = $('.m_in dt')[0];
	var s2 = $('.m_out dt')[0];
	var s3 = $('.mon dt span')[0];
	var load1 =load2=load3=0;
	var n1 = arr[0];
	var n2 = arr[1];
	var n3 = arr[2];
	var htmlnum;
	var htmlstr;
	timer=setInterval(function(){
		load1++;
		divL1.style.width =load1+'px';
		s1.innerHTML = Math.round(load1*n1/100);
		if(load1==100){clearInterval(timer);}
	},30);
	setTimeout(function(){
		timer2=setInterval(function(){
			load2++;
			htmlnum = Math.round(load2*n2/100);
			if(htmlnum>=60){
				htmlstr = Math.floor(htmlnum/60) + '`' + (htmlnum - Math.floor(htmlnum/60)*60);
			}else{
				htmlstr = htmlnum;
			}
			divL2.style.width = n1 > n2 ? Math.round(load2*0.5)+'px' : Math.round(load2*1.5)+'px';
			s2.innerHTML = htmlstr;
			if(load2==100){ 
				clearInterval(timer2);
			}
		},30);
	},delay);
	setTimeout(function(){
		timer3=setInterval(function(){
			load3++;
			s3.innerHTML = Math.round(load3*n3/100);
			if(load3==100){clearInterval(timer3);	}
		},30);
	},delay*2);
 }	
	