<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertBackendDefaultMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// 新增後台預設選單
		$sql = <<<SQL
TRUNCATE TABLE `admin_menu`;
SQL;
		DB::statement($sql);

		$sql = <<<SQL
INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '首页', 'fa-bar-chart', '/', NULL, NULL),
(2, 0, 2, '后台管理', 'fa-tasks', '', NULL, NULL),
(3, 2, 3, '管理者帐号', 'fa-users', 'auth/users', NULL, NULL),
(4, 2, 4, '角色设定', 'fa-user', 'auth/roles', NULL, NULL),
(5, 2, 5, '权限设定', 'fa-ban', 'auth/permissions', NULL, NULL),
(6, 2, 6, '菜单设定', 'fa-bars', 'auth/menu', NULL, NULL),
(7, 2, 7, '操作记录', 'fa-history', 'auth/logs', NULL, NULL),
(8, 0, 10, '等级管理', 'fa-user-secret', 'vipLevel', '2018-11-05 08:44:19', '2018-11-13 11:45:51'),
(9, 0, 11, '周工资', 'fa-money', 'weeklySalary', '2018-11-07 11:01:03', '2018-11-13 11:45:51'),
(10, 0, 12, '月薪水', 'fa-money', 'monthlySalary', '2018-11-07 11:01:40', '2018-11-13 11:45:51'),
(11, 0, 8, '会员管理', 'fa-user', 'memberBetting', '2018-11-08 07:17:04', '2018-11-14 02:47:15'),
(12, 0, 13, '升级奖金', 'fa-money', 'memberLevelUp', '2018-11-08 11:25:33', '2018-11-13 11:45:51'),
(13, 0, 14, '网站设置', 'fa-edge', 'settings', '2018-11-12 06:33:14', '2018-11-13 11:45:51'),
(14, 0, 15, '资料爬虫', 'fa-bug', 'crawler', '2018-11-12 06:54:07', '2018-11-13 11:45:51'),
(15, 0, 9, '会员投注查询', 'fa-user', 'memberBettingQuery', '2018-11-13 11:45:09', '2018-11-13 11:45:51');
SQL;
		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
