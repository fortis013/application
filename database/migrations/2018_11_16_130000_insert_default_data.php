<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// 新增 網站設定 資料
		$sql = <<<SQL
TRUNCATE TABLE `m_settings`;
SQL;
		DB::statement($sql);

		$sql = <<<SQL
INSERT INTO `m_settings` (`setting_id`, `title`, `demo_account`, `link1`, `link2`, `link3`, `link4`, `link5`, `link6`, `link7`, `m_link1`, `m_link2`, `m_link3`, `m_link4`, `m_link5`, `m_link6`, `created_at`, `updated_at`) VALUES
(1, '金沙赌场棋牌余额宝', '', 'https://tw.yahoo.com?a', 'https://tw.yahoo.com?b', 'https://tw.yahoo.com?c', 'https://tw.yahoo.com?d', 'https://tw.yahoo.com?e', 'https://tw.yahoo.com?f', 'https://tw.yahoo.com?g', '', '', '', '', '', '', '2018-11-12 14:41:07', '2018-11-13 11:28:54');
SQL;
		DB::statement($sql);


		// 新增 等級 資料
		$sql = <<<SQL
TRUNCATE TABLE `m_vip_level`;
SQL;
		DB::statement($sql);
		$sql = <<<SQL
INSERT INTO `m_vip_level` (`vip_level_id`, `dan`, `level`, `accum_amount`, `gift_money`, `weekly_salary`, `monthly_salary`, `created_at`, `updated_at`) VALUES
(1, '英勇黄铜', 1, 200000, 8, 0, 8, '2018-11-05 17:01:55', '2018-11-05 17:01:55'),
(2, '英勇黄铜', 2, 500000, 18, 0, 18, '2018-11-05 17:26:13', '2018-11-05 17:26:13'),
(3, '英勇黄铜', 3, 1000000, 38, 0, 58, '2018-11-05 18:13:56', '2018-11-05 19:41:49'),
(4, '英勇黄铜', 4, 5000000, 88, 10, 88, '2018-11-05 18:14:40', '2018-11-05 18:14:40'),
(5, '英勇黄铜', 5, 8000000, 188, 30, 128, '2018-11-05 18:15:13', '2018-11-05 18:15:13'),
(6, '英勇黄铜', 6, 11000000, 288, 48, 150, '2018-11-05 18:15:57', '2018-11-05 18:15:57'),
(7, '英勇黄铜', 7, 15000000, 688, 88, 250, '2018-11-05 18:16:47', '2018-11-05 18:16:47'),
(8, '英勇黄铜', 8, 19000000, 1500, 130, 300, '2018-11-05 18:17:46', '2018-11-05 18:17:46'),
(9, '英勇黄铜', 9, 21000000, 1800, 180, 350, '2018-11-05 18:18:53', '2018-11-05 18:18:53'),
(10, '英勇黄铜', 10, 23000000, 2000, 200, 400, '2018-11-05 18:19:37', '2018-11-05 18:19:37'),
(11, '不屈白银', 11, 25000000, 2200, 250, 450, '2018-11-05 18:21:31', '2018-11-05 18:21:31'),
(12, '不屈白银', 12, 27000000, 2500, 300, 500, '2018-11-05 18:22:14', '2018-11-05 18:22:14'),
(13, '不屈白银', 13, 29000000, 2800, 350, 600, '2018-11-05 18:22:50', '2018-11-05 18:22:50'),
(14, '不屈白银', 14, 31000000, 3000, 400, 700, '2018-11-05 18:23:54', '2018-11-05 18:23:54'),
(15, '不屈白银', 15, 33000000, 3200, 450, 800, '2018-11-05 18:24:33', '2018-11-05 18:24:33'),
(16, '不屈白银', 16, 35000000, 3500, 500, 900, '2018-11-05 18:25:31', '2018-11-05 18:25:31'),
(17, '不屈白银', 17, 37000000, 3800, 550, 1000, '2018-11-05 18:26:22', '2018-11-05 18:26:22'),
(18, '不屈白银', 18, 41000000, 4000, 600, 1200, '2018-11-05 18:27:29', '2018-11-05 18:27:29'),
(19, '不屈白银', 19, 43000000, 4200, 650, 1400, '2018-11-05 18:28:36', '2018-11-05 18:28:36'),
(20, '不屈白银', 20, 46000000, 4500, 700, 1600, '2018-11-05 18:29:16', '2018-11-05 18:29:16'),
(21, '荣耀黄金', 21, 49000000, 4800, 750, 1800, '2018-11-05 18:30:09', '2018-11-05 18:30:09'),
(22, '荣耀黄金', 22, 52000000, 5000, 800, 2100, '2018-11-05 18:32:03', '2018-11-05 18:32:03'),
(23, '荣耀黄金', 23, 55000000, 5200, 850, 2500, '2018-11-05 18:35:00', '2018-11-05 18:35:00'),
(24, '荣耀黄金', 24, 58000000, 5500, 900, 3000, '2018-11-05 18:35:51', '2018-11-05 18:35:51'),
(25, '荣耀黄金', 25, 62000000, 5800, 950, 3500, '2018-11-05 18:36:32', '2018-11-05 18:36:32'),
(26, '荣耀黄金', 26, 66000000, 6000, 1000, 4000, '2018-11-05 18:37:14', '2018-11-05 18:37:14'),
(27, '荣耀黄金', 27, 70000000, 6500, 1100, 4500, '2018-11-05 18:38:03', '2018-11-05 18:38:03'),
(28, '荣耀黄金', 28, 75000000, 7000, 1200, 5000, '2018-11-05 18:38:55', '2018-11-05 18:38:55'),
(29, '荣耀黄金', 29, 80000000, 7200, 1300, 5500, '2018-11-05 18:39:48', '2018-11-05 18:39:48'),
(30, '荣耀黄金', 30, 85000000, 7500, 1400, 6000, '2018-11-05 18:40:28', '2018-11-05 18:40:28'),
(31, '华贵铂金', 31, 90000000, 7800, 1500, 6500, '2018-11-05 18:41:14', '2018-11-05 18:41:14'),
(32, '华贵铂金', 32, 100000000, 8000, 1600, 7000, '2018-11-05 18:41:51', '2018-11-05 18:41:51'),
(33, '华贵铂金', 33, 110000000, 8200, 1700, 7500, '2018-11-05 18:43:53', '2018-11-05 18:43:53'),
(34, '华贵铂金', 34, 120000000, 8500, 1800, 8000, '2018-11-05 18:44:30', '2018-11-05 18:44:30'),
(35, '华贵铂金', 35, 140000000, 9000, 2000, 8500, '2018-11-05 18:45:12', '2018-11-05 18:45:12'),
(36, '华贵铂金', 36, 160000000, 10000, 2200, 10000, '2018-11-05 18:45:53', '2018-11-05 18:45:53'),
(37, '华贵铂金', 37, 180000000, 11000, 2400, 11000, '2018-11-05 18:46:56', '2018-11-05 18:46:56'),
(38, '华贵铂金', 38, 220000000, 12000, 2600, 12000, '2018-11-05 18:47:34', '2018-11-05 18:47:34'),
(39, '华贵铂金', 39, 300000000, 13000, 2800, 13000, '2018-11-05 18:48:23', '2018-11-05 18:48:23'),
(40, '华贵铂金', 40, 430000000, 14000, 3000, 14000, '2018-11-05 18:49:03', '2018-11-05 18:49:03'),
(41, '璀璨钻石', 41, 660000000, 15000, 3200, 15000, '2018-11-05 18:49:48', '2018-11-05 18:49:48'),
(42, '璀璨钻石', 42, 800000000, 16000, 3400, 16000, '2018-11-05 18:50:31', '2018-11-05 18:50:31'),
(43, '璀璨钻石', 43, 1330000000, 17000, 3600, 17000, '2018-11-05 18:51:26', '2018-11-05 18:51:26'),
(44, '璀璨钻石', 44, 1660000000, 18000, 3800, 18000, '2018-11-05 18:52:17', '2018-11-05 18:52:17'),
(45, '璀璨钻石', 45, 1990000000, 19000, 4000, 19000, '2018-11-05 18:53:09', '2018-11-05 18:53:09'),
(46, '璀璨钻石', 46, 2420000000, 21000, 4200, 20000, '2018-11-05 18:53:51', '2018-11-05 18:53:51'),
(47, '璀璨钻石', 47, 2550000000, 23000, 4400, 21000, '2018-11-05 18:54:53', '2018-11-05 18:54:53'),
(48, '璀璨钻石', 48, 2880000000, 25000, 4600, 22000, '2018-11-05 18:55:36', '2018-11-05 18:55:36'),
(49, '璀璨钻石', 49, 5000000000, 27000, 4800, 23000, '2018-11-05 18:56:26', '2018-11-06 10:41:36'),
(50, '璀璨钻石', 50, 8880000000, 30000, 5000, 25000, '2018-11-05 18:57:10', '2018-11-06 10:42:33'),
(51, '最强王者', 51, 10000000000, 35000, 6000, 28000, '2018-11-05 18:59:33', '2018-11-16 14:12:45');
SQL;
		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
