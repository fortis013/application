<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefault2Tables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// 建立 周工資 table
		$sql = <<<SQL
CREATE TABLE `d_weekly_salary` (
	`weekly_salary_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK',
	`title` VARCHAR(32) NOT NULL COMMENT '標題',
	`file` VARCHAR(64) NOT NULL COMMENT '檔案',
	`created_at` DATETIME NOT NULL COMMENT '建立日期',
	`updated_at` DATETIME COMMENT '更新日期',
	PRIMARY KEY (`weekly_salary_id`)
)
COMMENT='周工資'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);


		// 建立 月薪水 table
		$sql = <<<SQL
CREATE TABLE `d_monthly_salary` (
	`monthly_salary_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK',
	`title` VARCHAR(32) NOT NULL COMMENT '標題',
	`file` VARCHAR(64) NOT NULL COMMENT '檔案',
	`created_at` DATETIME NOT NULL COMMENT '建立日期',
	`updated_at` DATETIME COMMENT '更新日期',
	PRIMARY KEY (`monthly_salary_id`)
)
COMMENT='月薪水'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);


		// 建立 會員升級 table
		$sql = <<<SQL
CREATE TABLE `d_member_level_up` (
	`member_level_up_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK',
	`title` VARCHAR(32) NOT NULL COMMENT '標題',
	`file` VARCHAR(64) NOT NULL COMMENT '檔案',
	`quantity` SMALLINT UNSIGNED NOT NULL COMMENT '升級人數',
	`created_at` DATETIME NOT NULL COMMENT '建立日期',
	`updated_at` DATETIME COMMENT '更新日期',
	PRIMARY KEY (`member_level_up_id`)
)
COMMENT='會員升級'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
