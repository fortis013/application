<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// 建立 會員投注總合 table
		$sql = <<<SQL
CREATE TABLE `s_member_betting` (
	`account` VARCHAR(32) COLLATE utf8mb4_bin NOT NULL COMMENT '會員帳號',
	`betting_amount` DECIMAL(11, 3) NOT NULL DEFAULT 0 COMMENT '投注金額',
	`level` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '等級',
	`set_level` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '設定等級',
	`created_at` DATETIME NOT NULL COMMENT '建立日期',
	`updated_at` DATETIME COMMENT '更新日期',
	PRIMARY KEY (`account`)
)
COMMENT='會員投注總合'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);


		// 建立 會員投注資料 table
		$sql = <<<SQL
CREATE TABLE `d_member_betting` (
	`summary_date` DATE NOT NULL COMMENT '結算日期',
	`account` VARCHAR(32) COLLATE utf8mb4_bin NOT NULL COMMENT '會員帳號',
	`betting_amount` DECIMAL(11, 3) NOT NULL DEFAULT 0 COMMENT '投注金額',
	`created_at` DATETIME NOT NULL COMMENT '建立日期',
	`updated_at` DATETIME COMMENT '更新日期',
	PRIMARY KEY (`summary_date`, `account`)
)
COMMENT='會員投注資料'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);


		// 建立 VIP 等級 table
		$sql = <<<SQL
CREATE TABLE `m_vip_level` (
	`vip_level_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK',
	`dan` VARCHAR(8) NOT NULL COMMENT '段位',
	`level` TINYINT UNSIGNED NOT NULL COMMENT '等級',
	`accum_amount` BIGINT UNSIGNED NOT NULL COMMENT '累積投注金額',
	`gift_money` MEDIUMINT UNSIGNED NOT NULL COMMENT '等級禮金',
	`weekly_salary` MEDIUMINT UNSIGNED NOT NULL COMMENT '周工資',
	`monthly_salary` MEDIUMINT UNSIGNED NOT NULL COMMENT '月薪水',
	`created_at` datetime NOT NULL COMMENT '建立日期',
	`updated_at` datetime COMMENT '更新日期',
	PRIMARY KEY (`vip_level_id`)
)
COMMENT='VIP 等級'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
