<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// 修改 網站設定 (新增開新視窗設定欄位)
		$sql = <<<SQL
ALTER TABLE `m_settings`
	ADD `link1_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結一另開' AFTER `link7`,
	ADD `link2_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結二另開' AFTER `link1_blank`,
	ADD `link3_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結三另開' AFTER `link2_blank`,
	ADD `link4_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結四另開' AFTER `link3_blank`,
	ADD `link5_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結五另開' AFTER `link4_blank`,
	ADD `link6_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結六另開' AFTER `link5_blank`,
	ADD `link7_blank` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '連結七另開' AFTER `link6_blank`;
SQL;
		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
