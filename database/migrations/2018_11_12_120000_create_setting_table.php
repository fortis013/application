<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// 建立 網站設定 table
		$sql = <<<SQL
CREATE TABLE `m_settings` (
	`setting_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK',
	`title` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '網頁標頭',
	`demo_account` VARCHAR(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '測試帳號',
	`link1` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結一',
	`link2` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結二',
	`link3` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結三',
	`link4` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結四',
	`link5` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結五',
	`link6` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結六',
	`link7` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '連結七',
	`m_link1` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '手機連結一',
	`m_link2` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '手機連結二',
	`m_link3` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '手機連結三',
	`m_link4` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '手機連結四',
	`m_link5` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '手機連結五',
	`m_link6` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '手機連結六',
	`created_at` DATETIME NOT NULL COMMENT '建立日期',
	`updated_at` DATETIME COMMENT '更新日期',
	PRIMARY KEY (`setting_id`)
)
COMMENT='網站設定'
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB
;
SQL;
		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
