<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->get('crawler', 'CrawlerController@index');

	$router->post('memberBetting/reCalculate', 'MemberBettingController@reCalculate');
	$router->post('memberBettingQuery/import', 'MemberBettingQueryController@import');
	$router->post('memberBettingQuery/execCronJob', 'MemberBettingQueryController@execCronJob');

	$router->post('weeklySalary/download', 'WeeklySalaryController@download');
	$router->post('monthlySalary/download', 'MonthlySalaryController@download');
	$router->post('memberLevelUp/download', 'MemberLevelUpController@download');

	/*
	$router->post('weeklySalary/execCronJob', 'WeeklySalaryController@execCronJob');
	$router->post('monthlySalary/execCronJob', 'MonthlySalaryController@execCronJob');
	$router->post('memberLevelUp/execCronJob', 'MemberLevelUpController@execCronJob');
	*/

	$router->post('weeklySalary/execSummary', 'WeeklySalaryController@execSummary');
	$router->post('monthlySalary/execSummary', 'MonthlySalaryController@execSummary');
	$router->post('memberLevelUp/execSummary', 'MemberLevelUpController@execSummary');

	$router->resource('topbutton', TopbuttonController::class);
	$router->resource('news', NewsController::class);
	$router->resource('newoffer', NewofferController::class);
	$router->resource('index', IndexController::class);
	$router->resource('dayoffer', DayofferController::class);
	$router->resource('weekoffer', WeekofferController::class);
	$router->resource('monthoffer', MonthofferController::class);
	$router->resource('yearoffer', YearofferController::class);
	$router->resource('page', PageController::class);
	$router->resource('setting', SettingController::class);
	$router->resource('rightbutton', RightbuttonController::class);
	$router->resource('leftbutton', LeftbuttonController::class);
	$router->resource('top', TopController::class);
	$router->resource('down', DownController::class);
	$router->resource('nowonly', NowonlyController::class);
	});
