<?php

namespace App\Admin\Controllers;

use App\Model\down;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use Encore\Admin\Layout\Row; //使用Row $row 必须先引入
use Encore\Admin\Layout\Column; //使用Column $column 必须先引入

class DownController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('首页內容设置')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('首页內容设置')
            ->description('检视')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('首页內容设置')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('首页內容请设置')
            ->description('新建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new down);
		$grid->disableCreateButton();
	
		$grid->disableExport();
		// 關閉選擇器
		$grid->disableRowSelector();
		// 關閉搜尋
		$grid->disableFilter();
		// 關閉刪除按鈕
		
		$grid->actions(function ($actions) {
			/*
			$actions->disableEdit();
			*/
			$actions->disableView();
			
			$actions->disableDelete();
			
		});

		//$grid->column('setting_id', '编号');
		//$grid->column('demo_account', '测试帐号');
		
		/*
		$grid->column('link1', '立即注册');
		$grid->column('link2', '线路检测');
		$grid->column('link3', '资讯端下载');
		$grid->column('link4', '投诉建议');
		$grid->column('link5', '在线客服');
		$grid->column('link6', '立即加入');
		$grid->column('link7', '前往投注');
		$grid->column('link1_blank', '连结一另开');
		$grid->column('link2_blank', '连结二另开');
		$grid->column('link3_blank', '连结三另开');
		$grid->column('link4_blank', '连结四另开');
		$grid->column('link5_blank', '连结五另开');
		$grid->column('link6_blank', '连结六另开');
		$grid->column('link7_blank', '连结七另开');
		$grid->column('m_link1', '手机连结一');
		$grid->column('m_link2', '手机连结二');
		$grid->column('m_link3', '手机连结三');
		$grid->column('m_link4', '手机连结四');
		$grid->column('m_link5', '手机连结五');
		$grid->column('m_link6', '手机连结六');
		*/

		
		$grid->column('blank1', '一鍵入款连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('blank2', '十一大捕鱼机连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		
		$grid->column('blank3', '申请大厅连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('blank4', '天天红包连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('blank5', '手机下注连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('blank6', '满意度调查连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('blank7', '资讯端连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('blank8', '注册会员连结另开视窗')->switch([
		'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
		'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
		]);
		$grid->column('created_at', '建立日期');
		$grid->column('updated_at', '更新日期');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(down::findOrFail($id));

        $show->setting_id('Setting id');
        $show->title('Title');
        $show->demo_account('Demo account');
        $show->link1('Link1');
        $show->link2('Link2');
        $show->link3('Link3');
        $show->link4('Link4');
        $show->link5('Link5');
        $show->link6('Link6');
        $show->link7('Link7');
        $show->link1_blank('Link1 blank');
        $show->link2_blank('Link2 blank');
        $show->link3_blank('Link3 blank');
        $show->link4_blank('Link4 blank');
        $show->link5_blank('Link5 blank');
        $show->link6_blank('Link6 blank');
        $show->link7_blank('Link7 blank');
        $show->m_link1('M link1');
        $show->m_link2('M link2');
        $show->m_link3('M link3');
        $show->m_link4('M link4');
        $show->m_link5('M link5');
        $show->m_link6('M link6');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new down);

		$form->tools(function (Form\Tools $tools) {
			$tools->disableView();
			$tools->disableDelete();
			/*
			$tools->disableList();
			$tools->disableBackButton();
			$tools->disableListButton();
			*/
		});
		$form->tab('内容设定', function ($form) {
			
			$form->url('link1', '一鍵入款连结设定');
			$form->switch('blank1', '一鍵入款连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
			$form->url('link2', '十一大捕鱼机连结设定');
			$form->switch('blank2', '十一大捕鱼机连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
			$form->url('link3', '申请大厅连结设定');
			$form->switch('blank3', '申请大厅连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
			$form->url('link4', '天天红包连结设定');
			$form->switch('blank4', '天天红包连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]); 
			$form->url('link5', '手机下注连结设定');
			$form->switch('blank5', '手机下注连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
			$form->url('link6', '满意度调查连结设定');
			$form->switch('blank6', '满意度调查连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
			$form->url('link7', '资讯端连结设定');
			$form->switch('blank7', '资讯端连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
			$form->url('link8', '注册会员连结设定');
			$form->switch('blank8', '注册会员连结另开视窗')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
		});
		
        return $form;
    }
}
