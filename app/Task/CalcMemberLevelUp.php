<?php

namespace App\Task;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CalcMemberLevelUp extends TaskBase
{
	public function run()
	{
		// 結算日
		$target_date = date('Y-m-d', strtotime('-1 day'));


		DB::statement("SET SESSION sql_mode =''");

		$sql =
			"SELECT
				*,
				IF(s_betting_amount - betting_amount < accum_amount, 'Y', 'N') AS level_up
			FROM (
				SELECT
					m.summary_date,
					m.account,
					m.betting_amount AS betting_amount,
					sm.betting_amount AS s_betting_amount,
					MAX(v.level) AS level,
					MAX(v.accum_amount) AS accum_amount,
					MAX(v.gift_money) AS gift_money
				FROM d_member_betting AS m
					JOIN s_member_betting AS sm USING(account)
					JOIN m_vip_level AS v ON sm.betting_amount >= v.accum_amount
				WHERE
					m.summary_date =:target_date
				GROUP BY account
			) AS t
			HAVING level_up ='Y'
			ORDER BY s_betting_amount DESC";
		$rows = DB::select($sql, [
			'target_date' => $target_date,
		]);


		$rows_count = count($rows);
		if ($rows_count > 0) {
			$file = "level_up" . DIRECTORY_SEPARATOR . "{$target_date}.csv";
			Storage::disk('local')->put($file, '');
			$file_full_path = Storage::disk('local')->path($file);


			$handle = fopen($file_full_path, 'w');
			fputcsv($handle, ['帐号', '投注金额', '总投注金额', '等级', '等级礼金'], ',', '"');
			foreach ($rows as $row) {
				$data = [
					$row->account,
					$row->betting_amount,
					$row->s_betting_amount,
					$row->level,
					$row->gift_money
				];
				fputcsv($handle, $data, ',', '"');
			}
			fclose($handle);
		}

		$model = new \App\Model\DMemberLevelUp();
		$model->title = $target_date;
		$model->file = ($rows_count > 0) ? $file : '';
		$model->quantity = $rows_count;
		$model->save();


		return true;
	}

}
