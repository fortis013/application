<?php

namespace App\Task;

use Illuminate\Support\Facades\DB;

class CalcMemberBetting extends TaskBase
{
	public function run()
	{
		$target_date = date('Y-m-d 00:00:00');

		$users_betting = [];

		$tables = $this->getChessTables();
		foreach ($tables as $table) {
			// 計算昨天之前 handled = 0 的投注額
			$sql =
				"SELECT
					DATE(statistic_date) AS date_statistic,
					member_account,
					SUM(betting_valid) AS sum_betting_valid
				FROM {$table[0]}
				WHERE statistic_date < :target_date AND handled =0
				GROUP BY date_statistic, member_account";

			$rows = DB::connection('mysql2')->select($sql, [
				'target_date' => $target_date,
			]);

			foreach ($rows as $row) {
				if (! isset($users_betting[$row->date_statistic][$row->member_account])) {
					$users_betting[$row->date_statistic][$row->member_account] = 0;
				}
				$users_betting[$row->date_statistic][$row->member_account] += $row->sum_betting_valid;
			}
		}

		// 照日期排序 (Asc)
		uksort($users_betting, function ($a, $b) {
			if ($a == $b) {
				return 0;
			}
			if ($a > $b) {
				return 1;
			}
			return -1;
		});


		try {
			// Transaction begin 1 & 2
			DB::beginTransaction();
			DB::connection('mysql2')->beginTransaction();

			foreach ($users_betting as $date => $rows) {
				$arr_obj = new \ArrayObject($rows);
				$iterator = $arr_obj->getIterator();

				// Bulk insert
				while ($chunk = $this->getChunk($iterator)) {
					// 1
					$sql = "INSERT INTO s_member_betting (`account`, `betting_amount`, `created_at`) VALUES ";
					$values = array_fill(0, count($chunk), "(?, ?, ?)");
					$binds = [];
					foreach ($chunk as $k => $v) {
						$binds[] = $k;
						$binds[] = $v;
						$binds[] = $date . ' 00:00:00';
					}
					$sql .= implode(', ', $values);
					$sql .= " ON DUPLICATE KEY UPDATE `betting_amount` =`betting_amount` + VALUES(`betting_amount`), `updated_at` =VALUES(`created_at`)";
					DB::insert($sql, $binds);

					// 2
					$sql = "INSERT INTO d_member_betting (summary_date, account, betting_amount, created_at) VALUES ";
					$values = array_fill(0, count($chunk), "(?, ?, ?, NOW())");
					$binds = [];
					foreach ($chunk as $k => $v) {
						$binds[] = $date;
						$binds[] = $k;
						$binds[] = $v;
					}
					$sql .= implode(', ', $values);
					$sql .= " ON DUPLICATE KEY UPDATE betting_amount =betting_amount + VALUES(betting_amount), updated_at =NOW()";
					DB::insert($sql, $binds);
				}
			}


			// Update level
			$vip_levels = \App\Model\MVipLevel::select('level', 'accum_amount')
				->orderBy('level', 'ASC')
				->get();
			$count_vip_levels = count($vip_levels);

			$sql = "SELECT account, betting_amount FROM s_member_betting";
			$rows = DB::select($sql);

			// Bulk update
			$arr_obj = new \ArrayObject($rows);
			$iterator = $arr_obj->getIterator();
			while ($chunk = $this->getChunk2($iterator)) {
				$accounts = [];
				$binds = [];
				$sql = "UPDATE s_member_betting SET `level` =CASE";
				foreach ($chunk as $v) {
					$sql .= " WHEN account =? THEN ?";

					$level = 0;
					for ($i = 0; $i < $count_vip_levels; $i++) {
						if ($v->betting_amount >= $vip_levels[$i]->accum_amount) {
							continue;
						}
						if (isset($vip_levels[$i - 1])) {
							$level = $vip_levels[$i - 1]->level;
						}
						break;
					}

					$accounts[] = $v->account;
					$binds[] = $v->account;
					$binds[] = $level;
				}
				$sql .= " END WHERE account IN (" . implode(', ', array_fill(0, count($accounts), '?')) . ")";
				DB::update($sql, array_merge($binds, $accounts));
			}


			// Update handled = 1
			foreach ($tables as $table) {
				$sql = "UPDATE {$table[0]} SET handled =1 WHERE statistic_date < :target_date AND handled =0";

				$rows = DB::connection('mysql2')->update($sql, [
					'target_date' => $target_date,
				]);
			}


			// Transaction commit 1 & 2
			DB::commit();
			DB::connection('mysql2')->commit();

		} catch (\Exception $e) {
			DB::rollBack();
			DB::connection('mysql2')->rollBack();

			throw $e;
		}
	}

	protected function getChunk(\ArrayIterator &$arr_iterator, $chunk_size = 200)
	{
		$chunk = [];

		$i = 0;
		while ($arr_iterator->valid()) {
			$chunk[$arr_iterator->key()] = $arr_iterator->current();
			$arr_iterator->next();
			if (++$i >= $chunk_size) {
				break;
			}
		}

		return $chunk;
	}

	protected function getChunk2(\ArrayIterator &$arr_iterator, $chunk_size = 200)
	{
		$chunk = [];

		$i = 0;
		while ($arr_iterator->valid()) {
			$chunk[] = $arr_iterator->current();
			$arr_iterator->next();
			if (++$i >= $chunk_size) {
				break;
			}
		}

		return $chunk;
	}

}
