<?php

namespace App\Task;

use Illuminate\Support\Facades\DB;

class TaskBase
{
	public function getChessTables()
	{
		$sql = "SHOW TABLES LIKE 'day_chess_%'";
		$query = DB::connection('mysql2')->getPdo()->query($sql);
		return $query->fetchAll(\PDO::FETCH_NUM);
	}

}
