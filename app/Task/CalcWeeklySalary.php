<?php

namespace App\Task;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CalcWeeklySalary extends TaskBase
{
	public function run()
	{
		// 結算日
		$target_date = date('Y-m-d', strtotime('-1 day'));


		DB::statement("SET SESSION sql_mode =''");

		/*
		$sql =
			"SELECT
				m.account,
				m.betting_amount,
				MAX(v.level) AS level,
				MAX(v.accum_amount) AS accum_amount,
				MAX(v.weekly_salary) AS weekly_salary,
				MAX(v.monthly_salary) AS monthly_salary
			FROM s_member_betting AS m
				JOIN m_vip_level AS v ON m.betting_amount >= v.accum_amount
			GROUP BY m.account
			ORDER BY betting_amount DESC";
		*/
		$sql =
			"SELECT
				account,
				betting_amount,
				level,
				set_level,
				weekly_salary,
				monthly_salary
			FROM (
				SELECT
					m.account,
					m.betting_amount,
					m.level,
					m.set_level,
					@weekly_salary :=v.weekly_salary,
					@monthly_salary :=v.monthly_salary,
					@weekly_salary_2 :=v2.weekly_salary,
					@monthly_salary_2 :=v2.monthly_salary,
					IF(m.set_level > m.level, @weekly_salary_2, @weekly_salary) AS weekly_salary,
					IF(m.set_level > m.level, @monthly_salary_2, @monthly_salary) AS monthly_salary
				FROM s_member_betting AS m
					LEFT JOIN m_vip_level AS v ON m.level = v.level
					LEFT JOIN m_vip_level AS v2 ON m.set_level = v2.level
				WHERE m.level > 0 OR m.set_level > 0
				ORDER BY m.betting_amount DESC
			) AS t";
		$rows = DB::select($sql);


		$file = "weekly" . DIRECTORY_SEPARATOR . "{$target_date}.csv";
		Storage::disk('local')->put($file, '');
		$file_full_path = Storage::disk('local')->path($file);


		$handle = fopen($file_full_path, 'w');
		fputcsv($handle, ['帐号', '投注金额', '等级', '设定等级', '周工资'], ',', '"');
		foreach ($rows as $row) {
			if (! $row->weekly_salary) {
				continue;
			}
			$data = [
				$row->account,
				$row->betting_amount,
				$row->level,
				$row->set_level,
				$row->weekly_salary
			];
			fputcsv($handle, $data, ',', '"');
		}
		fclose($handle);


		$model = new \App\Model\DWeeklySalary();
		$model->title = $target_date;
		$model->file = $file;
		$model->save();


		return true;
	}

}
