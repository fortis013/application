<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

		// 計算每日投注額 (每天凌晨 2 點)
		$schedule->call(function () {
			(new \App\Task\CalcMemberBetting())->run();
		})->cron('0 2 * * *')->name('calc-betting-daily')->withoutOverlapping();

		// 計算升級獎金 (每天凌晨 2 點 30 分)
		$schedule->call(function () {
			(new \App\Task\CalcMemberLevelUp())->run();
		})->cron('30 2 * * 1')->name('member-level-up')->withoutOverlapping();

		// 計算週工資 (每週一凌晨 3 點)
		$schedule->call(function () {
			(new \App\Task\CalcWeeklySalary())->run();
		})->cron('0 3 * * 1')->name('weekly-salary')->withoutOverlapping();

		// 計算月薪水 (每月 1 號凌晨 4 點)
		$schedule->call(function () {
			(new \App\Task\CalcMonthlySalary())->run();
		})->cron('0 4 1 * *')->name('monthly-salary')->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
