<?php

namespace App\Observers;

use App\Model\DWeeklySalary;
use Illuminate\Support\Facades\Storage;

class DWeeklySalaryObserver
{
	// Handle the User "created" event.
	public function created(DWeeklySalary $model)
	{
	}

	// Handle the User "updated" event.
	public function updated(DWeeklySalary $model)
	{
	}

	// Handle the User "deleted" event.
	public function deleted(DWeeklySalary $model)
	{
		// 刪除實體檔案
		if (Storage::disk('local')->exists($model->file)) {
			Storage::disk('local')->delete($model->file);
		}
	}

}
