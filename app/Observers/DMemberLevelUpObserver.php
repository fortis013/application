<?php

namespace App\Observers;

use App\Model\DMemberLevelUp;
use Illuminate\Support\Facades\Storage;

class DMemberLevelUpObserver
{
	// Handle the User "created" event.
	public function created(DMemberLevelUp $model)
	{
	}

	// Handle the User "updated" event.
	public function updated(DMemberLevelUp $model)
	{
	}

	// Handle the User "deleted" event.
	public function deleted(DMemberLevelUp $model)
	{
		// 刪除實體檔案
		if (Storage::disk('local')->exists($model->file)) {
			Storage::disk('local')->delete($model->file);
		}
	}

}
