<?php

namespace App\Observers;

use App\Model\DMonthlySalary;
use Illuminate\Support\Facades\Storage;

class DMonthlySalaryObserver
{
	// Handle the User "created" event.
	public function created(DMonthlySalary $model)
	{
	}

	// Handle the User "updated" event.
	public function updated(DMonthlySalary $model)
	{
	}

	// Handle the User "deleted" event.
	public function deleted(DMonthlySalary $model)
	{
		// 刪除實體檔案
		if (Storage::disk('local')->exists($model->file)) {
			Storage::disk('local')->delete($model->file);
		}
	}

}
