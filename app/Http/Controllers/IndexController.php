<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
	// 首頁
	public function index(Request $request)
	{
		try {
			return view('index.index', $this->getVars($request));
		} catch (\Exception $e) {
			echo '网页发生错误 (' . $e->getMessage() . ')';
		}
	}

	protected function getVars(Request $request)
	{
	/*
	跑馬燈
	*/
		$sql =
			"
					SELECT link , content ,blank
					FROM news
					#ORDER BY setting_id 
					LIMIT 20
			";
		$news = DB::select($sql);
		
/*限時優惠*/
		$nowonly =
			"
					SELECT `name` , `link1` ,`blank1`,`link2`,`blank2`,`place`
					FROM nowonly
					ORDER BY place 
					
			";
		$nowonlys = DB::select($nowonly);
		
/*最新優惠*/
		$newoffer =
			"
					SELECT `name` , `link1` ,`blank1`,`link2`,`blank2`,`place`
					FROM newoffer
					ORDER BY place 
					
			";
		$newsoffer = DB::select($newoffer);
/*每日優惠*/
		
		$dayoffer =
			"
					SELECT `name` , `link1` ,`blank1`,`link2`,`blank2`,`place`
					FROM dayoffer
					ORDER BY place 
					
			";
		$daysoffer = DB::select($dayoffer);
		
/*每周優惠*/
		
		$weekoffer =
			"
					SELECT `name` , `link1` ,`blank1`,`link2`,`blank2`,`place`
					FROM weekoffer
					ORDER BY place 
					
			";
		$weeksoffer = DB::select($weekoffer);
		
/*每月優惠*/
		
		$monthoffer =
			"
					SELECT `name` , `link1` ,`blank1`,`link2`,`blank2`,`place`
					FROM monthoffer
					ORDER BY place 
					
			";
		$monthsoffer = DB::select($monthoffer);
		
/*每月優惠*/
		
		$yearoffer =
			"
					SELECT `name` , `link1` ,`blank1`,`link2`,`blank2`,`place`
					FROM yearoffer
					ORDER BY place 
					
			";
		$yearsoffer = DB::select($yearoffer);
		
/*頁面圖片*/
		$page =
			" 
					SELECT `img` , `link` ,`blank`
					FROM page
					ORDER BY id 
					
			";
		$pages = DB::select($page);
		
		

		
		$vars = [
			// TODO: 用 cache
			'setting' => \App\Model\setting::firstOrFail(),
			'news' => $news,
			'nowonlys' =>$nowonlys,
			'newsoffer' =>$newsoffer,
			'daysoffer' =>$daysoffer,
			'weeksoffer' =>$weeksoffer,
			'monthsoffer' =>$monthsoffer,
			'yearsoffer' =>$yearsoffer,
			'pages' =>$pages,
			'rightbutton'=> \App\Model\rightbutton::firstOrFail(),
			'leftbutton'=> \App\Model\leftbutton::firstOrFail(),
			'top'=> \App\Model\top::firstOrFail(),
			'down'=> \App\Model\down::firstOrFail(),
		];

		return $vars;
	}
	

}
