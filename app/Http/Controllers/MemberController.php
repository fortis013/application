<?php

namespace App\Http\Controllers;

use App\Model\MVipLevel;
use App\Model\SMemberBetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{

	// 會員資訊
	public function info(Request $request)
	{
		$res = ['error' => '', 'data' => '', 'msg' => ''];

		try {
			$account = trim($request->input('account'));
			if (! $account) {
				throw new \Exception('请输入会员帐号.', 101);
			}

			$member = SMemberBetting::where('account', '=', $account)->first();
			if (! $member) {
				throw new \Exception('无此会员.', 102);
			}

			$current_level = null;
			$next_level = null;

			$vip_levels = MVipLevel::select('dan', 'level', 'accum_amount', 'gift_money', 'weekly_salary', 'monthly_salary')
				->orderBy('level', 'ASC')
				->get();
			$count_vip_levels = count($vip_levels);
			for ($i = 0; $i < $count_vip_levels; $i++) {
				if ($member->betting_amount >= $vip_levels[$i]->accum_amount) {
					continue;
				}
				if (isset($vip_levels[$i - 1])) {
					$current_level = $vip_levels[$i - 1];
				}
				if (isset($vip_levels[$i])) {
					$next_level = $vip_levels[$i];
				}
				break;
			}

			$res['error'] = '000';
			$res['data'] = [
				'account' => $member->account,
				'betting_amount' => $member->betting_amount,
				'current_level' => $current_level ? $current_level->toArray() : '',
				'next_level' => $next_level ? $next_level->toArray() : '',
			];

		} catch (\Exception $e) {
			$res['error'] = $e->getCode();
			$res['msg'] = $e->getMessage();
		}

		return response()->json($res);
	}

}
