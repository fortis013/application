<?php

namespace App\Helper;

class Util
{
	public static function getChunk(\ArrayIterator &$arr_iterator, $chunk_size = 200)
	{
		$chunk = [];

		$i = 0;
		while ($arr_iterator->valid()) {
			$chunk[] = $arr_iterator->current();
			$arr_iterator->next();
			if (++$i >= $chunk_size) {
				break;
			}
		}

		return $chunk;
	}
}
