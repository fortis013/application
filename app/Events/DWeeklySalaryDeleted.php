<?php
die('Deprecated.');

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class DWeeklySalaryDeleted
{
	use SerializesModels;

	public $model;

	// Create a new event instance.
	public function __construct(\App\Model\DWeeklySalary $model)
	{
		$this->model = $model;
	}

}
